use std::
{
	fs::{ self, File }, 
	io::{ self, stdin, stdout, Write, BufRead, LineWriter }, 
	path::Path, 
	sync::{ Arc, Mutex}, 
	thread
};

fn main()
{
	let sub_en_path = Path::new("release/languageData/English");
	let mut installation_path = Path::new("C:\\Program Files (x86)\\Steam\\steamapps\\common\\TreeOfSavior");
	let mut s = String::new();
	loop
	{
		if installation_path.exists() && installation_path.join(sub_en_path).exists() { break; }
		println!("경로 {}가 올바르지 않습니다!", installation_path.to_str().unwrap());
		print!("설치 경로를 직접 입력해 주세요 (TreeOfSavior 폴더): ");
		let _ = stdout().flush();
		stdin().read_line(&mut s).expect("stdin 오류! 다른 터미널을 사용해 보세요.");
		installation_path = Path::new(s.trim());
	}
	let en_path = installation_path.join(sub_en_path);
	let ko_path = installation_path.join(Path::new("release/languageData/한국어"));
	if ko_path.exists()
	{
		fs::remove_dir_all(&ko_path).expect("이미 존재하는 한국어 폴더의 삭제에 실패했습니다! 패쳐를 관리자 권한으로 실행해 주세요!");
	}
	fs::create_dir(&ko_path).expect("한국어 폴더 생성에 실패했습니다! 패쳐를 관리자 권한으로 실행해 주세요!");
	let empty = Arc::new(Mutex::new(0));
	let untranslated = Arc::new(Mutex::new(0));
	let mut handles = Vec::new();
	for entry in en_path.read_dir().expect("경로를 읽는 데 실패했습니다!")
	{
		let entry = entry.unwrap();
		if !entry.file_type().unwrap().is_file() { continue; }
		match entry.path().extension()
		{
			None => continue,
			Some(os_str) =>
			{
				match os_str.to_str()
				{
					Some("tsv") => {},
					_ => continue
				};
			}
		};
		if entry.file_name() == "BADWORDS.tsv" { continue; }
		let ko_path = ko_path.clone();
		let empty = empty.clone();
		let untranslated = untranslated.clone();
		handles.push(thread::Builder::new().name(entry.file_name().to_str().unwrap().to_string()).spawn(move ||
		{
			println!("패치중: {:?}", entry.file_name());
			let file_ident = entry.file_name().to_str().unwrap().split('.').next().unwrap().split('_').next().unwrap().to_string();
			let en_file = File::open(entry.path()).unwrap();
			let ko_file_path = ko_path.to_owned().join(entry.file_name());
			let ko_file = File::create(ko_file_path).unwrap();
			let mut ko_file = LineWriter::new(ko_file);
			ko_file.write_all(b"\n").unwrap();
			for line in io::BufReader::new(en_file).lines()
			{
				let line = line.unwrap().trim().to_string();
				let mut cells = line.split('\t').collect::<Vec<&str>>();
				if cells.len() < 2
				{
					*empty.lock().unwrap() += 1;
					continue;
				}
				else if cells.len() == 2
				{
					*untranslated.lock().unwrap() += 1;
					ko_file.write_all(line.as_bytes()).unwrap();
					ko_file.write_all(b"\n").unwrap();

				}
				else if cells.len() > 3
				{
					let mut line = String::from(cells[0]);
					let mut skip_next = true;
					for (i, cell) in cells.iter().enumerate()
					{
						if cell.trim().is_empty() { continue; }
						if i == 0 { continue; }
						if skip_next { skip_next = false; continue; }
						if (*cell).starts_with(&file_ident)
						{
							ko_file.write_all(line.as_bytes()).unwrap();
							ko_file.write_all(b"\n").unwrap();
							line = String::from(*cell);
							skip_next = true;
						}
						else
						{
							line.push('\t');
							line.push_str(cell);
						}
					}
					ko_file.write_all(line.as_bytes()).unwrap();
					ko_file.write_all(b"\n").unwrap();
				}
				else
				{
					cells.remove(1);
					ko_file.write_all(cells.join("\t").as_bytes()).unwrap();
					ko_file.write_all(b"\n").unwrap();
				}
			}
			println!("패치 완료: {:?}", entry.file_name());
		}).unwrap());
	}
	for handle in handles
	{
		handle.join().unwrap();
	}
	println!("완료! 미번역 (번역 데이터 없음) {}줄, 빈 줄 {}줄", untranslated.lock().unwrap(), empty.lock().unwrap());
}